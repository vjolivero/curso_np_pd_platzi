# curso_np_pd_platzi



## Getting started

Este proyecto fue desarrollado como parte del cierre del curso "Manipulación y Transformación de Datos con Pandas y NumPy" de Platzi.

Contiene codigo fuente desarrollado en Jupyter Notebook y archivos csv como fuente de datos.

Creditos:

Solving real world data science tasks with Python Pandas! https://www.youtube.com/watch?v=eMOA1pPVUc4
https://github.com/KeithGalli/Pandas-Data-Science-Tasks

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
